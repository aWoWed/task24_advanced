﻿using System.Collections.Generic;
using System.Data.Entity;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Context
{
    /// <summary>
    /// Database Library Initializer
    /// </summary>
    public class LibraryContextInitializer : DropCreateDatabaseAlways<LibraryDbContext>
    {
        protected override void Seed(LibraryDbContext context)
        {
            context.Articles.AddRange(new List<Article> { _article1, _article2, _article3 });
        }

        #region Hardcoded data

        readonly Article _article1 = new Article
        {
            Name = "Epam",
            Text = "EPAM Systems, Inc. is an American company that specializes in product development, digital platform engineering, and digital and product design. " +
                   "The world's largest manufacturer of custom software, consulting specialist, resident of the Belarus High Technologies Park. " +
                   "The company's headquarters is located in Newtown, Pennsylvania, and its branches are represented in more than 30 countries."
        };

        readonly Article _article2 = new Article
        {
            Name = "Microsoft",
            Text = "Microsoft Corporation is an American multinational technology corporation which produces computer software, consumer electronics, personal computers, and related services. " +
                   "Its best known software products are the Microsoft Windows line of operating systems, the Microsoft Office suite, and the Internet Explorer and Edge web browsers. " +
                   "Its flagship hardware products are the Xbox video game consoles and the Microsoft Surface lineup of touchscreen personal computers.Microsoft ranked No. 21 in the 2020 Fortune " +
                   "500 rankings of the largest United States corporations by total revenue; it was the world's largest software maker by revenue as of 2016. " +
                   "It is considered one of the Big Five companies in the U.S.information technology industry, along with Google, Apple, Amazon, and Facebook."
        };

        readonly Article _article3 = new Article
        {
            Name = "Facebook",
            Text = "Facebook is an American online social media and social networking service owned by Facebook, Inc.Founded in 2004 by Mark Zuckerberg with fellow Harvard College students" +
                   " and roommates Eduardo Saverin, Andrew McCollum, Dustin Moskovitz, and Chris Hughes, its name comes from the face book directories often given to American university students. " +
                   "Membership was initially limited to Harvard students, gradually expanding to other North American universities and, since 2006, anyone over 13 years old.As of 2020, " +
                   "Facebook claimed 2.8 billion monthly active users, and ranked seventh in global internet usage. " +
                   "It was the most downloaded mobile app of the 2010s. Facebook can be accessed from devices with Internet connectivity, such as personal computers, tablets and smartphones. " +
                   "After registering, users can create a profile revealing information about themselves. They can post text, photos and multimedia " +
                   "which are shared with any other users who have agreed to be their friend or, with different privacy settings, publicly. " +
                   "Users can also communicate directly with each other with Facebook Messenger, join common - interest groups, and receive notifications on the activities of their Facebook friends and pages they follow. " +
                   "The subject of numerous controversies, Facebook has often been criticized over issues such as user privacy(as with the Cambridge Analytica data scandal), " +
                   "political manipulation(as with the 2016 U.S.elections), mass surveillance, psychological effects " +
                   "such as addiction and low self - esteem, and content such as fake news, conspiracy theories, copyright infringement, and hate speech. " +
                   "Commentators have accused Facebook of willingly facilitating the spread of such content, as well as exaggerating its number of users to appeal to advertisers."
        };

        #endregion
    }
}