﻿using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Articles page controller
    /// </summary>
    public class HomeController : Controller
    {
        private static readonly ArticleService ArticleService = new ArticleService();
        /// <summary>
        /// Loads Articles from Db
        /// </summary>
        /// <returns>Articles Page</returns>
        public ActionResult Index()
        {
            return View(ArticleService.Get());
        }
    }
}