﻿using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Comments page controller
    /// </summary>
    public class GuestController : Controller
    {
        private static readonly CommentService CommentService = new CommentService();
        /// <summary>
        /// Loads Comments from Db
        /// </summary>
        /// <returns>Comments Page</returns>
        public ActionResult Index()
        {
            return View(CommentService.Get());
        }

        /// <summary>
        /// Adds comment to Db
        /// </summary>
        /// <param name="comment"></param>
        /// <returns>Add comment</returns>
        [HttpPost]
        public ActionResult AddComment(CommentDTO comment)
        {
            if (string.IsNullOrEmpty(comment.Name))
                ModelState.AddModelError("Name", "Error! Please enter your name!");

            else if (comment.Name.Length < 2)
                ModelState.AddModelError("Name", "Error! Your nickname is too short(> 2 symbols)!");

            if (string.IsNullOrEmpty(comment.Text))
                ModelState.AddModelError("Text", "Error! Please enter your text!");

            if (ModelState.IsValid)
            {
                CommentService.InsertOrUpdate(comment);
                
                return RedirectToAction("Index");
            }

            return View("Index", CommentService.Get());
        }
    }
}
